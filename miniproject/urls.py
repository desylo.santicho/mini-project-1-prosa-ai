from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('account.api.urls')),
    path('api/posts/', include('post.api.urls', "post"), name="post"),
    path('api/posts/', include('comment.api.urls', "comment"), name="comment"),
    path('api/user/', include("account.api.urls"), name="account")
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
