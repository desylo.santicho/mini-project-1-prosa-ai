# Configuration Mini Project 1 `./config/.env`
The default value here is the value used if no value provided on environment variables (or `.env` file)

## Application Configuration
General app configuration

Variable      | Type         | Default     | Description |
------------- | -------------| ---------   | ----------- |
Content Cell  | str          | development | App environtment [development, staging, production, ..]  |
Content Cell  | str          |             | Secret Key for making hashes |

## Media Storage Configuration
Used for cloudinary, media cloud storage

Variable      | Type         | Default     | Description |
------------- | -------------| ---------   | ----------- |
API_KEY       | str          |             | Key for accessing Cloudinary storage |
API_SECRET    | str          |             | Secret Key for accessing Cloudinary storage |

## Database Configuration
Used Heroku PostgreSQL for staging & production

Variable      | Type         | Default     | Description |
------------- | -------------| ---------   | ----------- |
DATABASE_URL  | str          |             | Heroku PostgreSQL Database URL  |