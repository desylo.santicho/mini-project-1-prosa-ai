<div align="center">
    <a href="#">
    <img src="https://blog.prosa.ai/static/images/prosa/prosa-ai-black.png" alt="Logo" width="200px">
    </a>
</div>
  
<hr>

<!-- ABOUT THE PROJECT -->
## About The Project

Mini Project 1 is a Project for MSIB Intern to develop Message Board Application with an intern team consisting of Back-end Engineer and Front-end Engineer

### Technology Used

* [Python](https://www.python.org/)
* [Django](https://www.djangoproject.com/)
* [Django Rest Framework](https://www.django-rest-framework.org/)

<!-- GETTING STARTED -->
## Getting Started

Download the following requirements before starting local development

## Requirements
* [Pyenv](https://github.com/pyenv/pyenv) for Python Version Management
* Python 3.8.10
  * To install using pyenv
    ```
    pyenv install 3.8.10
    ```
* [Poetry](https://python-poetry.org/) for Python Package and Environment Management.
* [Git Clone](https://git-scm.com/docs/git-clone) for cloning this repository into your Device

## Local development

### Running the App

1. Run Poetry Shell to run the Virtual Environment
    ```
    poetry shell
    ```
2. Run Database Migration Script
    ```
    python manage.py migrate
    ```
3. Run Django Web Development Server
    ```
    python manage.py runserver
    ```

## Testing

### Setup

- Install Postman or other API testing tools
- Import the [OpenAPI schema files](api.json) to your API testing tools.

### API Testing

- Test all the available endpoints and HTTP Requests from the imported files

<!-- USAGE EXAMPLES -->
## Usage

Additional Usages

* Add Dockerfile script to build your application into docker image
* Please don't hardcode the configuration
* Try to implement clean code best practices
* Don't forget to add technical documentation and configuration details **(you're not immortal)**

<!-- ROADMAP -->
## Roadmap

See the [open issues](#) for a list of proposed features (and known issues).

<!-- MAINTAINERS -->
## Maintainer
* [Desylo Santicho](mailto:desylo.santicho@prosa.ai)

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
 List of library used

* [Markdown](https://www.markdownguide.org/)
* [Dockerfile](https://docs.docker.com/engine/reference/commandline/cli/)

## License
Copyright (c) 2022, [Prosa.ai](https://prosa.ai).