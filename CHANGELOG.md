# Changelog
All notable changes to this project will be documented in this file.

# [1.00] - 2022-03-1

### Added

### Api User endpoint
 * User Registration API:
   * Check username and email :
     * if username/email is already in use, return error
     * if password and confirmation password doesn't match, return error
 * Auth API:
   * Token auth
   * Get access token
 * User API:
   * Get all users
   * RUD Profile (Delete for deleting the user)
     * Object permission/authorization for Delete
 * Profile API:
   * Profile picture validatin: 
     * max file upload is 100kKB
     * Object permission/authorization for Update

### API post endpoint

 * Message API:
   * Get all messages
   * CRUD Message
   * Object permission/authorization for Update & Delete

### API comment endpoint

 * Comment API:
   * Get all comments
   * CRUD Comment
   * Object permission/authorization for Update & Delete 